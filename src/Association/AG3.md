## Planification

Il faut prévoir une date et un lieu.
Pour la date, la 1ère semaine de mai permettrait de tenir notre intervalle de deux mois.
Pour le lieu, considérant le confinement, ce sera très probablement sur le Jitsi Deuxfleurs.

Ordre du jour :

  - Debrief des deux mois
    - Déploiement du site web
    - Déploiement et debug du Jitsi 

*N'hésitez pas à compléter ce document*
