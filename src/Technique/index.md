Deuxfleurs utilise les composants suivants dans son infrastructure:

- Ansible (configuration des noeuds)
- Docker (conteneurs)
- Nomad (orchestration des conteneurs)
- Consul (stockage clef/valeur distribué, découverte de services)
- Glusterfs (système de fichiers distribué)
- Stolon (système de réplication pour PostgreSQL)

Les services proposés sont les suivants:

- Chat via Matrix (Synapse, Riot)
- Email (Postfix, Dovecot, SoGo)
- Stockage (Seafile)

Par ailleurs, nous avons développés nous-même un certain nombre d'outils pour compléter la stack:

- [Bottin](https://bottin.eu), un serveur LDAP (gestion des comptes utilisateurs) basé sur le stockage clef/valeur de Consul
- [Guichet](https://git.deuxfleurs.fr/Deuxfleurs/Guichet/), une interface web de gestion des utilisateurs
- [Easybridge](https://git.deuxfleurs.fr/lx/Easybridge/), un bridge entre Matrix et d'autres réseaux

Le code de l'infrastructure [est publiquement disponible](https://git.deuxfleurs.fr/Deuxfleurs/deuxfleurs.fr/).
